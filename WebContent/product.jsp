<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">

<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<meta charset="utf-8">

<style type="text/css">
.header, .message {
	margin-bottom: 20px;
}

th, td {
	text-align: center;
}
</style>

</head>
<body background="${contextPath}/img/3.jpg">
		<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<nav class="navbar navbar-expand-lg navbar-light bg-info">
		<div class="container-fluid">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<form id="logoutForm" method="POST" action="${contextPath}/logout">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</c:if>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				
					<li class="nav-item active"><a href="${contextPath}/home">
					<button class="btn btn-info btn-lg" type="submit"><h4>
						Home</h4></button>
				</a></li>
					

					<li class="nav-item"><a href="${contextPath}/products">
					<button class="btn btn-info btn-lg" type="submit"><h4>
						Products</h4></button>
				</a></li>
				</ul>
			
            
		    
   
				<a href="${contextPath}/invenotytracking">
					<button class="btn btn-info btn-lg" type="submit"><h4>Inventory
						Tracking</h4></button>
				</a> <a href="${contextPath}/salestracking">
					<button class="btn btn-info btn-lg" type="submit">
        <h4>Sales Tracking</h4></button>
				</a>
				 <div class="nav-item dropdown">
			<a class="nav-link dropdown-toggle btn btn-info btn-lg" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          ${pageContext.request.userPrincipal.name} 
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" data-toggle="collapse"
					onclick="document.forms['logoutForm'].submit()">
					
					<button class="btn btn-danger btn-lg" type="submit">Logout</button>
				</a>
				
        </div>
        </div>
		</div>
		</div>	
	</nav>

		
		
		
	<br>
	<br>
	<br>
	<font size="4"> 
<div class="container" align="center">
    
    <div class="row">
        <div class="col-sm-10 py-8">
            <div class="card card-body h-500">
	
	
		<table class="table table-hover container" cellpadding="10px" cellspacing="0px">

			<thead>
				<tr>
					<th colspan="7" style="text-align: center;"><h2>
							<i class="fa fa-product-hunt"></i> Product List
						</h2></th>
				</tr>
				<tr>
					<th>Product_Id</th>
					<th>Product_Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Vendor Details</th>
					<th>Edit Details</th>
					<th>delete Details</th>
				</tr>

			</thead>
			<c:forEach var="j" items="${p}">
				<tbody>
					<tr>
						<td>${j.product_id}</td>
						<td>${j.product_name}</td>
						<td>${j.price}</td>
						<td>${j.quantity_recived}</td>
						<td>${j.vendor_name}</td>
						<td><a href="${contextPath}/${j.product_id}"
							class="btn btn-primary btn-lg">Edit</a></td>
						<td>
							<form action="${contextPath}/${j.product_id}/delete" method="get" >
								<input type="submit" class="btn btn-danger btn-lg" value="Delete" onclick="return confirm('Are you want to delete this item?')" />
							</form>
						</td>
					</tr>
				</tbody>
			</c:forEach>
		</table>
		
	<div style="padding-left:10px;">
		<a href="${contextPath}/add-products">
			<button class="btn btn-primary btn-lg" type="submit">Add New
				Products</button>
		</a>

	</div>
	
	</div></div></div></div>
	</font>
</body>
</html>
