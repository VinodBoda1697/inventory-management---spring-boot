<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<BODY background="img/SSG-Food-Market-Landini-Associates-Trevor-Mein-Carpark-Entrance-2-1280x960.jpg"></BODY>
<meta charset="utf-8">
<title>Welcome</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-info">
		<div class="container-fluid">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<form id="logoutForm" method="POST" action="${contextPath}/logout">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</c:if>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
				
					<li class="nav-item active"><a href="${contextPath}/home">
					<button class="btn btn-info btn-lg" type="submit"><h3>
						Home</h3></button>
				</a></li>
					

					<li class="nav-item"><a href="${contextPath}/products">
					<button class="btn btn-info btn-lg" type="submit"><h3>
						Products</h3></button>
				</a></li>
				</ul>
				<a href="${contextPath}/invenotytracking">
					<button class="btn btn-info btn-lg" type="submit"><h3>Inventory
						Tracking</h3></button>
				</a> <a href="${contextPath}/salestracking">
					<button class="btn btn-info btn-lg" type="submit">
        <h3>Sales Tracking</h3></button>
				</a>
				 <div class="nav-item dropdown">
			<a class="nav-link dropdown-toggle btn btn-info btn-lg" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <font size="4">${pageContext.request.userPrincipal.name}</font> 
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" data-toggle="collapse"
					onclick="document.forms['logoutForm'].submit()">
					
					<button class="btn btn-danger btn-lg" type="submit">Logout</button>
				</a>
				
        </div>
        </div>
		</div>
		</div>	
	</nav>
<div class="mx-auto" style="width: 200px;height:500px;">
  
<br><br><br><br><br>
	<header>
		<a href="${contextPath}/add-customer"><button
				class="btn btn-white-50 bg-primary btn-lg"><h4>Add Customer</h4></button></a>
	</header>
</div>

</body>
</html>