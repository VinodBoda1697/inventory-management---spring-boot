<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Create an account</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			<form id="logoutForm" method="POST" action="${contextPath}/logout">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</form>

			<h2>
				Welcome ${pageContext.request.userPrincipal.name} | <a
					onclick="document.forms['logoutForm'].submit()">Logout</a>
			</h2>
		</c:if>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
     <div style="float: left; margin-right: 90px;">
			<a
				href="${contextPath}/home">
				<button class="btn btn-primary" type="submit">home</button>
			</a>

		</div>

	<br>
	<br>
	<table class="table table-hover container" border="2px"
		cellpadding="10px" cellspacing="0px">
		<thead>
			<tr>
				<th>Product Name</th>
				<th>Quantity</th>
				<th>Price</th>
			</tr>
		</thead>
						<c:forEach var="j" items="${p}">
	        
			<tbody>
			<tr>
					<td>${j.product_name}</td>
					<td>${j.quantity_recived}</td>
					<td>${j.price}</td>
									
				</tr>
	</tbody>
	</c:forEach>
	</table>
