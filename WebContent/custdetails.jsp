<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<html lang="en">
<head>
<BODY
	background="img/3.jpg"></BODY>

<style type="text/css">
.table .thead-dark th {
width: 100% !important;
	color: #fff;
	/* background-color: #C42B23 !important; */
	border-color: #454d55 !important;
}

element.style {
	
}

.table {
	width: 55%
	margin-bottom: 1rem;
	color: primary !important;
	align-self: center;
	
}

.Aligner {
	display: flex;
	align-items: center;
	justify-content: center;
}

.Aligner-item {
	max-width: 50%;
}

.Aligner-item--top {
	align-self: flex-start;
}

.Aligner-item--bottom {
	align-self: flex-end;
}
.navbar-toggle .icon-bar{
  width: 1000px !important;
  height:3px !important;
}
</style>
<!-- <BODY background="img/Minimalism Simple Abstract Green Background wallpaper  3.jpg"></BODY> -->
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CUstomer Details</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body>


	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
	<nav class="navbar navbar-expand-lg navbar-light bg-info">
		<div class="container-fluid">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<form id="logoutForm" method="POST" action="${contextPath}/logout">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</c:if>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">

					<li class="nav-item active"><a href="${contextPath}/home">
							<button class="btn btn-info btn-lg" type="submit">
								<h6>Home</h6>
							</button>
					</a></li>


					<li class="nav-item"><a href="${contextPath}/products">
							<button class="btn btn-info btn-lg" type="submit">
								<h6>Products</h6>
							</button>
					</a></li>
				</ul>




				<a href="${contextPath}/invenotytracking">
					<button class="btn btn-info btn-lg" type="submit">
						<h6>Inventory Tracking</h6>
					</button>
				</a> <a href="${contextPath}/salestracking">
					<button class="btn btn-info btn-lg" type="submit">
						<h6>Sales Tracking</h6>
					</button>
				</a>
				<div class="nav-item dropdown">
					<a class="nav-link dropdown-toggle btn btn-info btn-lg" href="#"
						id="navbarDropdown" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						${pageContext.request.userPrincipal.name}
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" data-toggle="collapse" 
							onclick="document.forms['logoutForm'].submit()">

							<button class="btn btn-danger" type="submit">
								<h6>Logout</h6>
							</button>
						</a>

					</div>
				</div>
			</div>
		</div>
	</nav>
<br><br><br><br>
<div >
<div class="container" align="center">
    
    
    <div class="row">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;
        <div class="col-sm-7 py-4">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;
            <div class="card card-body h-100">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;
            	<div class="Aligner">
		<div class="Aligner-item Aligner-item--top">
		<h3>Customer Details</h3>
		<form:form style="font-weight: bold;" action="${contextPath}/search"
			modelAttribute="c" method="post">
			<table class="table table-hover container" cellpadding="10px" cellspacing="0px" border="1">
				<thead >

					<tr>
						<th scope="col">Customer Name</th>
						
						<th>${c.customer_name}</th>
					</tr>
					<tr>
						<th scope="col">Mobile Number</th>
					
						<th>${c.mobile}<br></th>
					</tr>
					<tr>
						<th scope="col">Address</th>
						<th>${c.address}<br></th>
					</tr>
					<tr>
						<th scope="col">Pincode</th>
						
						<th>${c.pincode}</th>
					</tr>
			</table>
			<br><br>
			<div align="center">
				<input class="btn btn-info" type="submit"
					value="Start Shopping">
			</div>
		</form:form>
		</div>
	</div>
	</div>
        </div>
	
</div></div>
</div>
</body>
</html>