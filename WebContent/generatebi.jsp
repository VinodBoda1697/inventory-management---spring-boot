
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Create an account</title>
<link href="${contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet">
</head>
<body>
	<script>
		function myFunction() {
			window.print();
		}
	</script>
	<div align="justify">
		<h3>
			<a href="${contextPath}/welcome">Home</a>
		</h3>
	</div>

	<div align="center">
		<br /> <br />
		<div align="center">
			<h2>
				<b>Annapurna Inventory Managements</b>
			</h2>
		</div>
		<br /> <font size=4">

			<div align="right">
				<h4>
					Ordering Date:<%=(new java.util.Date()).toLocaleString()%></h4>
				<br>
				<h2>Customer Details</h2>

				

					<c:forEach var="j" items="${c}">
						<tr>

							<th>Customer Name:</th>
							<td>${j.customer_name}</td>
						</tr>
						<br>
						<tr>
							<th>Mobile:</th>
							<td>${j.mobile}</td>
						</tr>
						<br>
						<tr>
							<th>Address:</th>
							<td>${j.address}</td>
						</tr>

					</c:forEach>
		</font></div>
	</div>

	</div>

	<div align="left">
		<img src="img/logo.png" class="img-fluid" height="100px" width="150px">
		<h4>Annapurna Inventory Solutions</h4>
		<h4>Yadagirigutta,</h4>
		<h4>508115.</h4>
		<h4>India.</h4>
		<h4>Phone Number:9848012345</h4>
		<!-- <h5>Hitech City,Hyderabad,</h5> -->
	</div>
	<div>

		<div>
			<div>
				<div></div>
			</div>
			<br />
		</div>
	</div>
	<br />
	<br />

	<div class="center">
		<div>
			<div>
				<div style="padding-left:600px">
					<h2>Order List</h2>
				</div>
				<br />
			</div></div>
			<div>
				<table border="2" width=50% align="center">
					<tr>


						<td>Product Name</td>
						<td>Quantity Required</td>
						<td>Price Per Unit</td>
						<td>Total Price</td>
					</tr>
					<c:forEach var="j" items="${pr}">
						<tr>
							<%
								int total = 0;
							%>

							<td align="center">${j.product_name}</td>
							<td align="center">${j.quantity}</td>
							<td align="center">${j.ind_price}</td>
							<td align="center">${j.price}</td>
						</tr>
						<tr>


							<c:set var="total" value="${total+j.price}" />


						</tr>
					</c:forEach>

				</table>
</div></div>
				<div align="center">
				<br>
				<br>
				<br> total:<input type="text" name="tot" id="tot"
					value="<c:out value="${total}"/>">
				<div align="center">
					<button onclick="myFunction()" class="btn btn-primary">Print
						Bill</button>
				</div>
			</div>
		</div>
	</div>
	
</body>
</html>