<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<html lang="en">
<head>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Employee</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<BODY
	background="img/3.jpg"></BODY>
<%@ page isELIgnored="false"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Customer</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Popper JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<style>


input[type=text], select, textarea {
	width: 100%;
	padding: 12px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	resize: vertical;
	/*  float: center; */
}

/* Style the label to display next to the inputs */
label {
	padding: 12px 12px 12px 0;
	display: inline-block;
	color: white;
	/* float:center; */
}

/* Style the submit button */
input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 12px 20px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
	/* float: left; */
}

/* Style the container */
.container {
	border-radius: 10px;
	background-color:;
	padding: 20px;
	width: 100%;
	height: 70%;
	/* float: center; */
}

/* Floating column for labels: 25% width */
.col-25 {
	float: center;
	width: 25%;
	margin-top: 6px;
}

/* Floating column for inputs: 75% width */
.col-75 {
	float: center;
	width: 75%;
	margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
	content: "";
	display: table;
	clear: both;
/* 	float: center; */
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
/* @media screen and (max-width: 600px) {
	.col-25, .col-75, input[type=submit] {
		width: 100%;
		margin-top: 0;
	}
} */
label {
    padding: 12px 12px 12px 0;
    display: inline-block;
    color: lightgoldenrodyellow;
    font-weight: bold;
}
</style>


</head>
<body>
<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

	<nav class="navbar navbar-expand-lg navbar-light bg-info">
		<div class="container-fluid">
			<c:if test="${pageContext.request.userPrincipal.name != null}">
				<form id="logoutForm" method="POST" action="${contextPath}/logout">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</c:if>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">

					<li class="nav-item active"><a href="${contextPath}/home">
							<button class="btn btn-info btn-lg" type="submit">
								<h6>Home</h6>
							</button>
					</a></li>


					<li class="nav-item"><a href="">
							<button class="btn btn-info btn-lg" type="submit">
								<h6>Products</h6>
							</button>
					</a></li>
				</ul>




				<a href="${contextPath}/invenotytracking">
					<button class="btn btn-info btn-lg" type="submit">
						<h6>Inventory Tracking</h6>
					</button>
				</a> <a href="${contextPath}/salestracking">
					<button class="btn btn-info btn-lg" type="submit">
						<h6>Sales Tracking</h6>
					</button>
				</a>
				 <div class="nav-item dropdown">
					<a class="nav-link dropdown-toggle btn btn-info btn-lg" href="#"
						id="navbarDropdown" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						${pageContext.request.userPrincipal.name}
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" data-toggle="collapse" 
							onclick="document.forms['logoutForm'].submit()">

							<button class="btn btn-danger" type="submit">
								<h6>Logout</h6>
							</button>
						</a>

					</div>
				</div>
				 
		</div>
	</nav>

<div align="center">
		<form:form style="" action="${contextPath}/add"
			modelAttribute="p" method="post">


			 <div class="col-25" align="center">
				<form:label path="product_name">Product Name</form:label>
				<form:input class="form-control" type="text" id="name" path="product_name" />
				<form:errors path="product_name" />
			</div>
		 <div class="col-25" align="center">
				<form:label path="quantity_recived">Quantity Recived</form:label>
				<form:input class="form-control" type="text" id="qty" path="quantity_recived" />
				<form:errors path="quantity_recived" />
			</div>
			<br>
			 <div class="col-25" align="center">
				<form:label path="price">Price</form:label>
				<form:input class="form-control" type="text" id="price" path="price" />
				<form:errors path="price" />
			</div>
			 <div class="col-25" align="center">
				<form:label path="Vendor_name">Vendor Name</form:label>
				<form:input class="form-control" type="text" id="vname" path="Vendor_name" />
				<form:errors path="Vendor_name" />
			</div>
		
			
			<br>
			<br>
			<br>
			<div>
				<input class="btn btn-info btn-lg" type="submit"
					value="Add Product">
			</div>
		</form:form>
		</div>
	
</body>
</html>
