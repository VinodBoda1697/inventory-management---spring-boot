package trg.talentsprint.starterkit.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice")

public class Invoice {

	@Id

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long invoicenumber;
	long custid;

	public long getInvoicenumber() {
		return invoicenumber;
	}

	public void setInvoicenumber(long invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public long getCustid() {
		return custid;
	}

	public void setCustid(long custid) {
		this.custid = custid;
	}

	@Override
	public String toString() {
		return "Invoice [invoicenumber=" + invoicenumber + ", custid=" + custid + "]";
	}
}
