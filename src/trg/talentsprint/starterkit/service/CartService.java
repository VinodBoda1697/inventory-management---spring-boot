package trg.talentsprint.starterkit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Cart;
import trg.talentsprint.starterkit.model.Products;
import trg.talentsprint.starterkit.repository.CartRepository;


@Service
	public class CartService {

	private static CartRepository rep;
	@Autowired
    public CartService(CartRepository repository) {
        CartService.rep= repository;
    }
	
	public static  Cart save(Cart stock) {
        return rep.save(stock);
    }
	public static List<Cart> getDetailsByCustid(long custid){
		return rep.getDetailsByCustid(custid);
	
	}
	public static  List<Cart> findAll() {
        return (List<Cart>) rep.findAll();
    }
	public static List<Cart> searchOrderDate(String startdate,String lastdate)
	{
		return (List<Cart>) rep.searchOrderDate(startdate, lastdate);
	}
	public static List<Cart> findPrice(long lowprice,long highprice)
	{
		return (List<Cart>) rep.findPrice(lowprice, highprice);
	}

	public static  List<Cart> findProductByName(String name) {
        return (List<Cart>) rep.findByNames(name);
    }
	
	}