package trg.talentsprint.starterkit.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Products;
import trg.talentsprint.starterkit.repository.ProductRepository;

@Service
	public class ProductService {

	    private static ProductRepository rep;
		//public static Object getEmployeeByDesignation;

	    @Autowired
	    public ProductService(ProductRepository repository) {
	        ProductService.rep = repository;
	    }
	    public static  Products save(Products stock) {
	        return rep.save(stock);
	    }
	    public static  List<Products> findAll() {
	        return (List<Products>) rep.findAll();
	    }
	    public static Optional<Products> findById(Long id) {
	        return rep.findById(id);
	    }
	    public static void deleteById(Long id) {
	        rep.deleteById(id);
	    }
	    public static List<Products> findProductByName(String name) {
			return rep.findByProductName(name);
		}
	    public static void updateQuantity(long quant)
	    {
	    	rep.updateQuantity(quant);
	    }

}

