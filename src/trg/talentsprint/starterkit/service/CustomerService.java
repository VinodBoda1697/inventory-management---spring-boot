package trg.talentsprint.starterkit.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Customers;
import trg.talentsprint.starterkit.model.Products;
import trg.talentsprint.starterkit.repository.CustomerRepository;

@Service
public class CustomerService {
	 private static CustomerRepository rep;
	 @Autowired
	    public CustomerService(CustomerRepository repository) {
	        CustomerService.rep = repository;
	    }
	 
	 public static Customers save(Customers stock) {
	        return rep.save(stock);
	    }
	 public static Optional<Customers> findById(Long id) {
	        return rep.findById(id);
	    }
	 public static  List<Customers> findByCustomerId(Long id) {
		 return rep.findByCustomerId(id);
	}
}
