package trg.talentsprint.starterkit.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trg.talentsprint.starterkit.model.Invoice;
import trg.talentsprint.starterkit.model.Products;
import trg.talentsprint.starterkit.repository.InvoiceRespository;

@Service

public class InvoiceService {
	private static InvoiceRespository rep;
	@Autowired
    public InvoiceService(InvoiceRespository repository) {
		InvoiceService.rep= repository;
    }
	public static Invoice save(Invoice stock) {
        return rep.save(stock);
    }
	public static Optional<Invoice> findById(Long id) {
        return rep.findById(id);
    }
}
