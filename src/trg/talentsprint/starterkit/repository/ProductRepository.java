package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

///package trg.talentsprint.starterkit.repository;

//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import trg.talentsprint.starterkit.model.Products;

@Repository
public interface ProductRepository extends CrudRepository<Products, Long> {
	
	@Query("select p from Products p where p.product_name  like :name%")
	List<Products> findByProductName(@Param("name") String name);
	
	
	@Modifying
	@Query("update Products p set p.quantity_recived = p.quantity_recived-:quant")
	void updateQuantity(@Param("quant") long quantity_recived);
}
