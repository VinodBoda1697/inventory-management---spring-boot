package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import trg.talentsprint.starterkit.model.Cart;
import trg.talentsprint.starterkit.model.Products;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {

	@Query("SELECT c FROM Cart c where c.cust_id=?1")
	public List<Cart> getDetailsByCustid(long custid);

	@Query(value = "select * from cart where date between :startdate and :enddate ", nativeQuery = true)
	public List<Cart> searchOrderDate(@Param("startdate") String startdate, @Param("enddate") String enddate);

	@Query(value = "select * from cart where price between :lowprice and :highprice", nativeQuery = true)
	List<Cart> findPrice(@Param("lowprice") long lowprice, @Param("highprice") long highprice);
    
	@Query("select p from Cart p where p.product_name  like :name%")
	List<Cart> findByNames(@Param("name") String name);
}
