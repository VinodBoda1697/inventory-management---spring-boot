package trg.talentsprint.starterkit.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import trg.talentsprint.starterkit.model.Customers;

@Repository
public interface CustomerRepository extends  CrudRepository<Customers, Long> {
	@Query(value="select * from customers where customer_id=?1"   ,nativeQuery=true)
	List<Customers> findByCustomerId(Long id);
}
