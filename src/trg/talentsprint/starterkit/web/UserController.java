package trg.talentsprint.starterkit.web;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import trg.talentsprint.starterkit.model.Cart;
import trg.talentsprint.starterkit.model.Customers;
import trg.talentsprint.starterkit.model.Invoice;
import trg.talentsprint.starterkit.model.Products;
import trg.talentsprint.starterkit.model.User;
import trg.talentsprint.starterkit.service.CartService;
import trg.talentsprint.starterkit.service.CustomerService;
import trg.talentsprint.starterkit.service.InvoiceService;
import trg.talentsprint.starterkit.service.ProductService;
import trg.talentsprint.starterkit.service.SecurityService;
import trg.talentsprint.starterkit.service.UserService;
import trg.talentsprint.starterkit.validator.UserValidator;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @GetMapping({"/","/welcome"})
    public String welcome(Model model) {
    	model.addAttribute("p", ProductService.findAll());
    	return "welcome";
    }
    @GetMapping({"/home"})
    public String home(Model model) {
    	
    	return "welcome";
    }
    @GetMapping("/products")
    public String products(Model m)
    {
    	//m.addAttribute("p", new Products());
    	m.addAttribute("p", ProductService.findAll());
		return "product";

    }
    @GetMapping("/add-products")
    public String addProducts(Model m)
    {
    	m.addAttribute("p", new Products());
    	//m.addAttribute("p", ProductService.findAll());
		return "new-product";

    }
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addNewProduct(@Valid @ModelAttribute Products p, BindingResult result, Model model) {
    	
    	if (result.hasErrors()) {
			return "new-product";
		}
    	ProductService.save(p);
		model.addAttribute("p", ProductService.findAll());
		return "welcome";
	}
    @GetMapping("/add-customer")
    public String addCustomers(Model m)
    {
    	m.addAttribute("c", new Customers());
		return "customer";

    }
    @PostMapping(value = "/add-cus", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public String addNewCustomer(@Valid @ModelAttribute Customers c, BindingResult result, Model model,HttpSession http) {
    
    	if (result.hasErrors()) {
			return "customer";
		}
    	
    	CustomerService.save(c);
    	long id=c.getCustomer_id();
    	///System.out.println(id);
    	http.setAttribute("id", id);
    	model.addAttribute("c",c);
	return "custdetails";
	}
    @GetMapping("/{id}")
	public String showCustomerById(@PathVariable Long id, Model model) {
		
    	Products p = ProductService.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		model.addAttribute("p", p);
		return "edit-product";
	}

	@PostMapping("/{id}/update")
	public String updateBook(@PathVariable("id") Long product_id, @Valid @ModelAttribute Products p, BindingResult result,
			Model model) {
		
		if (result.hasErrors()) {
			return "edit-product";
		}
		String s=p.getProduct_name();
		long i=p.getPrice();
		long i1=p.getQuantity_recived();
		String s1=p.getvendor_name();
		Products p1;
		p1=ProductService.findById(product_id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + product_id));
		p1.setProduct_name(s);
		p1.setPrice(i);
		p1.setQuantity_recived(i1);
		p1.setVendor_name(s1);
		ProductService.save(p1);
		model.addAttribute("p", ProductService.findAll());
		return "product";
	}
	@GetMapping("/{id}/delete")
	public String deleteProduct(@PathVariable Long id, Model model) {
		ProductService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
		ProductService.deleteById(id);
		model.addAttribute("p", ProductService.findAll());
		return "product";
	}
	@PostMapping("/search")
	public String search(Model m)
	{   
		return "search";
	}
	@GetMapping("/searchresults")
	public String searchResults(@RequestParam(name = "custid") long custid,@RequestParam(name = "name") String name,Model m)
	{  
		m.addAttribute("p",ProductService.findProductByName(name));
		m.addAttribute("c", CartService.getDetailsByCustid(custid));
		 return "searchre";
	}
	@GetMapping("/addtocart")
	public String addCart(@RequestParam(name = "to") long q,@RequestParam(name = "proid") long id,@RequestParam(name = "proname") String pname,@RequestParam(name = "quant") long quant,@RequestParam(name = "total") long total,@RequestParam(name = "custid") long custid,@RequestParam(name = "ip") long iprice,Model m,@ModelAttribute Products p,HttpSession http) {
	Cart c= new Cart(); 
	//System.out.println(total);
	http.setAttribute("tot", total);  
Products p1=ProductService.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid book Id:" + id));
p1.setQuantity_recived(q);
ProductService.save(p1);
	c.setProduct_id(id);
	c.setProduct_name(pname);
	c.setQuantity(quant);
	c.setPrice(total);
	c.setCust_id(custid);
	c.setInd_price(iprice);
	CartService.save(c);
	m.addAttribute("c", CartService.getDetailsByCustid(custid));

	return "searchre";
	}
	@GetMapping("/invoice")
	public String invoicegen(@RequestParam(name = "custid") long custid,Model m)
	{   
	    Invoice i=new Invoice();
	    i.setCustid(custid);
	    InvoiceService.save(i);
	    //System.out.println(i1.getInvoicenumber());
	    m.addAttribute("i",i);
		return "Invoicegeneration";
	}
	@GetMapping("/generatebill")
	public String billgeneration(@Valid @ModelAttribute Invoice i,Model m,@RequestParam(name = "custid") long custid,@RequestParam(name = "iid") long iid,@RequestParam(name = "iid") long total)
	{   i.setCustid(custid);
	    i.setInvoicenumber(iid);  
	long id=i.getCustid();
	    //System.out.println(i.getInvoicenumber());
	    
	  m.addAttribute("pr",CartService.getDetailsByCustid(custid)); 
	   m.addAttribute("c",CustomerService.findByCustomerId(id));
		return "generatebi";
	}
	@GetMapping("/invenotytracking")
    public String trackInventory(Model m)
    {
		m.addAttribute("p", ProductService.findAll());		
		
     return "showproductdetails";
    }
	@GetMapping("/searchbyname")
    public String searchByName(Model m,@RequestParam(name = "name") String name)
    {
		//System.out.println(name);
		m.addAttribute("p", ProductService.findProductByName(name));		
		
     return "showproductdetails";
    }
	@GetMapping("/searchbynames")
    public String searchByNames(Model m,@RequestParam(name = "name") String name)
    {
		//System.out.println(name);
		m.addAttribute("c", CartService.findProductByName(name));		
		
     return "sales";
    }
	@GetMapping("salestracking")
	public String trackSales(Model m)
	{
		m.addAttribute("c", CartService.findAll());
		return "sales";
	}
	 @GetMapping("/searchbydate")
	 public String searchOrderDates(Model model,@RequestParam(name="startdate") String startdate,@RequestParam(name="enddate") String enddate){
		List<Cart> cart = CartService.searchOrderDate(startdate,enddate);
		
		 model.addAttribute("c", cart);
    
    	
			 return "sales";
		 
  }
  @GetMapping("/searchbyprice")
	 public String searchPrice(Model model,@RequestParam(name="lowprice") long lowprice,@RequestParam(name="highprice") long highprice) {
		List<Cart> cart = CartService.findPrice(lowprice, highprice);
		model.addAttribute("c", cart);
			 return "sales";
		 
  }
}